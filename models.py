__author__ = 'AlexanderSivtsev'

from sqlalchemy import Column, Integer, String, Text, ForeignKey

from database import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    login = Column(String(20), unique=False)
    first_name = Column(String(20), unique=False)
    second_name = Column(String(20), unique=False)
    last_name = Column(String(20), unique=False)
    password = Column(String(30), unique=False)
    _class = Column(String(20), unique=False)
    school = Column(Text, unique=False)
    birthday = Column(String(20), unique=False)
    teacher = Column(String(20), unique=False)

    def __init__(self, login=None, first_name=None, second_name=None, last_name=None, password=None, _class=None,
                 school=None,
                 birthday=None, teacher=None):
        self.login = login
        self.first_name = first_name
        self.second_name = second_name
        self.last_name = last_name
        self.password = password
        self._class = _class
        self.school = school
        self.birthday = birthday
        self.teacher = teacher

    def __repr__(self):
        return '<User %r>' % self.id


class WorkTask(Base):
    __tablename__ = 'tasks'
    id = Column(Integer, primary_key=True)
    code = Column(String(10), unique=True)
    name = Column(String(50), unique=True)
    brief_name = Column(String(50), unique=True)
    input_file = Column(String(50), unique=False)
    output_file = Column(String(50), unique=False)
    timeout = Column(Integer, unique=False)
    text = Column(Text, unique=False)

    def __init__(self, code=None, name=None, brief_name=None, input_file=None, output_file=None, timeout=None, text=None):
        self.code = code
        self.name = name
        self.brief_name = brief_name
        self.input_file = input_file
        self.output_file = output_file
        self.timeout = timeout
        self.text = text

    def __repr__(self):
        return '<Task %r>' % self.id


class Work(Base):
    __tablename__ = 'works'
    id = Column(Integer, primary_key=True)
    title = Column(String(50))

    def __init__(self, title=None):
        self.title = title

    def __repr__(self):
        return '<Work %r>' % self.id


class WorksTasks(Base):
    __tablename__ = 'works_tasks'
    id = Column(Integer, primary_key=True)
    work_id = Column(ForeignKey('works.id'))
    task_id = Column(ForeignKey('tasks.id'))

    def __init__(self, work_id=None, task_id=None):
        self.work_id = work_id
        self.task_id = task_id

    def __repr__(self):
        return '<WorksTasks %r>' % self.id


class Group(Base):
    __tablename__ = 'groups'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Group %r>' % self.id


class GroupsWorks(Base):
    __tablename__ = 'groups_works'
    id = Column(Integer, primary_key=True)
    group_id = Column(ForeignKey('groups.id'))
    work_id = Column(ForeignKey('works.id'))

    def __init__(self, group_id=None, work_id=None):
        self.group_id = group_id
        self.work_id = work_id

    def __repr__(self):
        return '<GroupsWorks %r>' % self.id


class Result(Base):
    __tablename__ = 'results'
    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('users.id'))
    works_tasks_id = Column(ForeignKey('works_tasks.id'))
    scores = Column(Integer)

    def __init__(self, user_id=None, task_id=None, scores=None):
        self.user_id = user_id
        self.task_id = task_id
        self.scores = scores

    def __repr__(self):
        return '<Result %r>' % self.id
