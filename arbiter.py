# -*- coding: utf-8 -*-
""" Arbiter - это модуль проверки олимпиадных задач для Multimeter """
import shutil
import subprocess
import sys
import time
from argparse import ArgumentParser
from functools import reduce

from multimeter import tasks, languages
from multimeter.helpers import *

from models import Result, User, WorkTask
from database import db_session

class Task:
    """ Задача """

    def __init__(self, code, task, work_dir):
        """ Инициализация задания """
        self.code = code
        self.timeout = task['timeout']
        self.input_file = task['input_file']
        self.output_file = task['output_file']
        self.task_dir = join(work_dir, self.code)
        self.solutions_dir = join(self.task_dir, 'solutions')
        self.preliminary_dir = join(self.task_dir, 'preliminary')
        self.test_suites_dir = join(self.task_dir, 'tests')
        if not os.path.isdir(self.task_dir):
            raise Exception('Task {} folder not found: {}'.format(self.code, self.task_dir))
        check_or_create_dir(self.solutions_dir)
        check_or_create_dir(self.test_suites_dir)

        self.test_suites = task['test_suites']
        self.preliminary = task['preliminary']

        for test in self.preliminary:
            self.verify_test(test)

        total_score = 0
        for suite_code, suite in self.test_suites.items():
            if suite['scoring'] == 'entire':
                total_score += suite['total_score']
            elif suite['scoring'] == 'partial':
                total_score += suite['test_score'] * len(suite['tests'])

            for test in suite['tests']:
                self.verify_test(test, suite_code)

        if total_score != 100:
            raise Exception('Sum of tests score of task {} not equal 100 !!!'.format(self.code))

    def verify_test(self, test, suite_code=None):
        """ Проверка теста
        :param test: имя теста
        :param suite_code:
        """
        if suite_code is None:
            test_name = "Preliminary test {}".format(test)
            input_file = join(self.preliminary_dir, test)
        else:
            test_name = "Test {} in {}".format(test, suite_code)
            input_file = join(self.test_suites_dir, suite_code, test)
        answer_file = input_file + '.a'
        if not os.path.isfile(input_file):
            print(input_file)
            raise Exception('{} for task {} not found !!!'.format(test_name, self.code))
        if not os.path.isfile(answer_file):
            raise Exception('{} for task {} don\'t have answer !!!'.format(test_name, self.code))

        try:
            subprocess.check_call(
                [join(self.task_dir, "check.exe"), input_file, answer_file, answer_file],
                stderr=subprocess.DEVNULL,
                stdout=subprocess.DEVNULL)
        except FileNotFoundError:
            raise Exception('Checker for task {} is not found !!!'.format(self.code))
        except subprocess.CalledProcessError:
            raise Exception('Checker for task {} is not working !!!'.format(self.code))

    def check_test(self, language, solution, test_file):
        """ Проверка решения на одном тесте
        :param language:
        :param solution:
        :param test_file:
        """
        # Запуск
        print("###############################################################################")
        print(solution + " test: " + test_file)
        print("###############################################################################")
        shutil.copy(test_file, self.input_file)
        try:
            subprocess.check_call(language.execution.format(solution=solution), timeout=self.timeout)
        except subprocess.CalledProcessError:
            return 'RE'  # Runtime error
        except subprocess.TimeoutExpired as e:
            print("### Killing " + e.cmd)
            subprocess.call("TaskKill /IM {}.exe /F".format(solution))
            # subprocess.call("tskill {}".format(solution))  # Windows XP
            return 'TL'  # Time Limit Exceeded
        finally:
            print("### Done")

        # Проверка
        shutil.copy(test_file + '.a', 'answer.txt')
        try:
            subprocess.check_call('..\\check.exe {input} {output} answer.txt'.format(
                input=self.input_file,
                output=self.output_file))
        except subprocess.CalledProcessError as error:
            if error.returncode == 1:
                return 'WA'  # Wrong answer
            elif error.returncode == 2:
                return 'PE'  # Presentation error
            else:
                return '??'  # Unknown error
        self.cleanup()
        return 'OK'

    def cleanup(self):
        """ Очистка после проверки теста """
        cleanup_list = [self.input_file, self.output_file, 'answer.txt']
        while len(cleanup_list) > 0:
            for filename in cleanup_list:
                try:
                    os.remove(filename)
                    cleanup_list.remove(filename)
                except FileNotFoundError:
                    cleanup_list.remove(filename)
                except PermissionError:
                    pass


class Arbiter:
    """ Арбитр для проверки решений """

    def __init__(self):
        """ Установка параметров, проверка каталогов, компиляторов и заданий """
        try:
            parser = ArgumentParser(description='Арбитр для проверки олимпиадных задач по программированию')
            parser.add_argument('-w', '--work', default='work', type=str, help="рабочий каталог")
            params = parser.parse_args()

            print("Verifying folders...")
            self.base_dir = os.getcwd()
            self.work_dir = join(self.base_dir, params.work)
            if not os.path.isdir(self.work_dir):
                raise Exception("Work folder {} not found !!!".format(self.work_dir))

            self.queue_dir = join(self.work_dir, '.queue')
            check_or_create_dir(self.queue_dir)

            self.results_dir = join(self.work_dir, '.results')
            check_or_create_dir(self.results_dir)

            self.users = self.load_config('users.json')

            print("Verifying languages...", end='')
            languages.verifying()
            print("ok")

            print("Verifying tasks:")
            # tasks = self.load_config('tasks.json')
            self.tasks = {}
            for code, task in tasks.items():
                self.tasks[code] = Task(code, task, self.work_dir)
                print('  {}... ok'.format(code))

            self.log = open(join(self.work_dir, 'arbiter.log'), "a")
            self.write_log("Arbiter started.")

        except Exception as error:
            sys.exit("ERROR: " + error.args[0])

    def __del__(self):
        try:
            self.log.close()
        except AttributeError:
            pass

    def write_log(self, msg):
        """ Запись сообщения в лог
        :param msg:
        """
        self.log.write(datetime.now().strftime('%Y-%m-%dT%H:%M:%S') + "  " + msg + "\n")

    def check_solution(self, task, language, solution):
        """ Проверка решений
        :param task:
        :param language:
        :param solution:
        """
        answer = {
            "datetime": datetime.now().strftime('%Y-%m-%dT%H:%M:%S'),
            "language": language.name,
            "revealed": False,
            "compilation": "CE",
            "preliminary": [],
            "results": {}
        }
        # Компиляция
        os.chdir(task.solutions_dir)

        self.write_log("\n**********************************\n" + task.code + ":" + solution + '.' + language.extension)
        try:
            self.write_log("Compiling ...")
            subprocess.check_call(language.batch + ' ' + solution)
            # for command in language.compilation:
            #     subprocess.check_call(command.format(solution=solution))
            answer['compilation'] = 'OK'
            self.write_log("Compiled OK")
        except subprocess.CalledProcessError:
            self.write_log("Compile failed")
            return answer

        # Предварительная проверка
        for test in task.preliminary:
            self.write_log("Checking preliminary test " + task.code + ':' + test)
            test_file = join(task.preliminary_dir, test)
            answer['preliminary'].append({
                "test": test,
                "result": task.check_test(language, solution, test_file)
            })
            task.cleanup()

        for preliminary in answer['preliminary']:
            if preliminary['result'] != 'OK':
                self.write_log("Failed preliminary test " + preliminary['test'] + ", aborting")
                return answer
        self.write_log("Passed preliminary tests")

        # Проверка на основных тестах
        for suite_key, suite_value in task.test_suites.items():
            self.write_log("Starting test group " + suite_key)
            answer['results'][suite_key] = []
            for test in suite_value['tests']:
                test_file = join(task.test_suites_dir, suite_key, test)
                self.write_log("Starting test " + test)
                current_test_result = task.check_test(language, solution, test_file)
                answer['results'][suite_key].append({
                    "test": test,
                    "result": current_test_result
                })
                self.write_log("Test " + test + ", result: " + current_test_result)
                if not os.path.exists(task.output_file):
                    self.write_log("Test " + test + ", result file is not exist")
                elif current_test_result == 'WA' or current_test_result == 'PE':
                    ouf = open(task.output_file, "r")
                    txt = ouf.read(30).replace('\n', '\\n')
                    ouf.close()
                    if len(txt) == 30:
                        txt += '...'
                    msg = 'Found: \n' + txt + '\nexpected:\n'

                    anf = open('answer.txt', "r")
                    txt = anf.read(30).replace('\n', '\\n')
                    if len(txt) == 30:
                        txt += '...'
                    msg += txt + '\n'
                    anf.close()
                    self.write_log(msg)
                self.log.flush()
                task.cleanup()

        return answer

    def queue_is_empty(self):
        """ Компиляция и проверка загруженных решений """
        for filename in os.listdir(self.queue_dir):
            (task_code, username, lang_code, attempt) = filename.split('-')
            task = self.tasks[task_code]
            language = languages[lang_code]
            solution = '{}-{}'.format(username, attempt)

            # Перемещение файла из очереди в папку решений
            source_file = join(self.queue_dir, filename)
            destination_file = join(task.solutions_dir, "{}.{}".format(solution, language.extension))
            shutil.move(source_file, destination_file)

            result = self.check_solution(task, language, solution)

            total = 0
            for key, values in result['results'].items():
                settings = task.test_suites[key]
                if settings['scoring'] == 'entire':
                    total += reduce(lambda a, b: a if b['result'] == 'OK' else 0, values, settings['total_score'])
                elif settings['scoring'] == 'partial':
                    total += reduce(lambda a, b: a + settings['test_score'] if b['result'] == 'OK' else a, values, 0)
            result['total'] = total

            os.chdir(self.base_dir)

            workTask = WorkTask.query.filter_by(code=task_code).first()
            user = User.query.filter_by(login=username).first()
            result = Result(user.id, workTask.id, total)
            db_session.add(result)
            db_session.commit()

            save_json(result, join(self.results_dir, '{}-{}-{}.json'.format(task_code, username, attempt)))

        return True

    def run_arbiter(self):
        """ Ожидание появления новых решений в очереди """
        try:
            print('Arbiter is ready, press Ctrl+C to stop')
            while self.queue_is_empty():
                time.sleep(1)
        except KeyboardInterrupt:
            print('Arbiter has been stopped')

    def load_config(self, filename):
        """ Загрузка конфигурационного файла
        :param filename:
        """
        full_path = join(self.work_dir, filename)
        print("Loading settings from {0}... ".format(full_path), end='')
        cur_config = load_json(full_path)
        print("ok")
        return cur_config


def load_json(filename, default=None):
    """ Загрузка данных из конфигурационного файла
    :type filename:
    :param default:
    """
    try:
        # Если в файле есть BOM - убираем его
        raw = open(filename, 'rb').read()
        if raw.startswith(codecs.BOM_UTF8):
            data_file = open(filename, mode='wb')
            data_file.write(raw.decode("utf-8-sig").encode("utf-8"))
            data_file.close()

        # Чтение файла в OrderedDict
        data_file = open(filename, encoding='UTF-8')
        data = json.load(data_file, object_pairs_hook=collections.OrderedDict)
        data_file.close()

    except FileNotFoundError:
        if default is None:
            print('File {} not found'.format(filename))
            raise FileNotFoundError
        data = default

    except ValueError:
        if default is None:
            print('File {} is corrupted'.format(filename))
            raise ValueError
        data = default

    return data


def save_json(data, filename):
    """ Сохранение данных в конфигурационный файл
    :param data:
    :param filename:
    """
    data_file = open(filename, mode='w', encoding='UTF-8')
    json.dump(data, data_file, ensure_ascii=False, indent='\t')
    data_file.close()


if __name__ == '__main__':
    arbiter = Arbiter()
    arbiter.run_arbiter()
