#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>

using namespace std;

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	string s;

	int n, m;

	cin >> m >> n;

	double a[20];

	for (int j = 0; j < n; j++)
	{
		a[j] = 0;
	}

	for (int i = 0; i < m; i++)
	{
		cin >> s;
		cin >> s;
		cin >> s;

		int tmp;
		for (int j = 0; j < n; j++)
		{
			cin >> tmp;
			a[j] += tmp;
		}
		cin >> tmp;
	}

	double myMin = a[0] / m;
	int myIndex = 0;

	for (int j = 0; j < n; j++)
	{
		a[j] /= m;
		if (myMin > a[j])
		{
			myMin = a[j];
			myIndex = j;
		}
	}

	printf("%d %.4lf", ++myIndex, myMin);

	return 0;
}