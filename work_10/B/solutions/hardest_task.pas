program hardest_task;
var
  a : array[1..20] of longint;
  input, output : text;
  n, m, i, j, k, p, min, mini : longint;
  c : char;
begin
  assign(input, 'input.txt'); reset(input);
  assign(output, 'output.txt'); rewrite(output);

  readln(input, n, m);
  for i:=1 to 20 do a[i]:=0;
  for i:=1 to n do
  begin
    p:=0;
    while p<3 do begin read(input, c); if c=' ' then inc(p); end;
    for j:=1 to m do begin read(input, k); a[j]:=a[j]+k; end;
    readln(input, k);
  end;
  min:=a[1]; mini:=1;
  for i:=2 to m do
    if a[i]<min then begin min:=a[i]; mini:=i; end;
  writeln(output, mini, ' ', (min/n):3:3);

  close(input);
  close(output);
end.
