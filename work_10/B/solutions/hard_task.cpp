#include <fstream>
#include <string>

using namespace std;

int main(int argc, char* argv[])
{
    ifstream in("input.txt");
    ofstream out("output.txt");

    int N, M, i, j, score;
    in >> M >> N;
    double *sum = new double[N];
    for (j=0; j<N; ++j){
        sum[j] = 0;
    }

    string f, im, o;
    for (i=0; i<M; ++i) {
        in >> f >> im >> o;
        for (j=0; j<N; ++j){
            in >> score;
            sum[j] += score;
        }
        in >> score; // ����� ������
    }

    double minsum = sum[0];
    int minj = 0;
    for (j=1; j<N; ++j){
    	if (sum[j] < minsum) {
    		minsum = sum[j];
    		minj = j;
    	}
    }

    out.precision(4);
    out << minj+1 << " " << fixed << minsum/M;
}

