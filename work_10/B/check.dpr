{$APPTYPE CONSOLE}
uses testlib, sysutils;
var i,j,n,m,b,minj,ansminj:integer; sum: array[1..20] of extended; 
    min, ansmin:extended;
    sp:set of char;
Begin
   m:=inf.ReadInteger;
   n:=inf.ReadInteger;
   for j:=1 to N do 
     sum[j]:=0;
   sp:= [' ', #09];
   for i:=1 to M do
   begin
     inf.ReadWord(sp,sp);inf.ReadWord(sp,sp);inf.ReadWord(sp,sp);
     for j:=1 to N do 
     begin
	b := inf.ReadInteger; 
        sum[j] := sum[j] + b;
     end;
     inf.ReadInteger;
   end;

   minj := ouf.ReadInteger;
   ansminj := ans.ReadInteger;
   min := ouf.ReadReal;
   ansmin := ans.ReadReal;

   if (abs(min-ansmin)>0.001) then quit(_wa, 'Wrong minimum, expected <' +FloatToStr(ansmin)+ '>, found <' +FloatToStr(min)+ '>!')
   else
      if minj=ansminj then quit(_ok, 'OK')
      else 
        if (abs(sum[minj]/M - ansmin)>0.001) then quit(_wa, 'Wrong task number!')      
                                             else quit(_ok, 'OK');
End.