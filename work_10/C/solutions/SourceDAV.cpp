#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>

using namespace std;

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	string s;

	int n, m;

	cin >> m >> n;

	int a[105][105];

	for (int i = 0; i < 105; i++)
	{
		for (int j = 0; j < 105; j++)
		{
			a[i][j] = 0;
		}
	}

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			cin >> a[i][j];
		}
	}

	int k = 0;

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			if (a[i][j] == 0 && (a[i + 1][j] || a[i - 1][j] || a[i][j + 1] || a[i][j - 1]))
			{
				k++;
			}
		}
	}

	cout << k;

	return 0;
}