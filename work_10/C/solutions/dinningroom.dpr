program dinningroom;
{$APPTYPE CONSOLE}
uses
  sysutils;
var
  a : array[0..101, 0..101] of integer;
  n, m, i, j, k : integer;
begin
  assign(input, 'input.txt'); reset(input);
  assign(output, 'output.txt'); rewrite(output);
  readln(n, m);
  for i:=0 to 101 do
    for j:=0 to 101 do
      a[i,j] := 0;
  for i:=1 to m do
    for j:=1 to n do
      read(a[i,j]);
  k := 0;
  for i:=1 to m do
    for j:=1 to n do
    begin
      if (a[i,j]=0) and ((a[i-1,j]=1) or (a[i+1,j]=1) or (a[i,j-1]=1) or (a[i,j+1]=1)) then
        inc(k);
    end;
  writeln(k);

  close(input);
  close(output);
end.
