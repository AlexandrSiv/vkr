program mult_subtask1;
{$APPTYPE CONSOLE}
uses
  sysutils;

var
  i, j, n, li, nechet, nechet_i : integer;
  a : array[1..1000] of integer;
  s1, s2, s3 : string;
begin
  assign(input, 'input.txt'); reset(input);
  assign(output, 'output.txt'); rewrite(output);
  readln(n);
  for i:=1 to 1000 do a[i]:=0;
  for i:=1 to n do
  begin
    read(li);
    inc(a[li]);
  end;
  s1:='';
  s2:='';
  s3:='';
  for i:=1 to 1000 do
    if a[i] mod 2 <> 0 then begin inc(nechet); nechet_i:=i; end;
  if nechet>1 then writeln('No')
  else
  begin
    for i:=1 to 1000 do
      if i<>nechet_i then
      begin
        for j:=1 to (a[i] div 2) do
        begin
          s1 := s1 + inttostr(i) + ' ';
          s2 := inttostr(i) + ' ' + s2;
        end;
      end
      else
        for j:=1 to a[i] do
          s3:=s3+inttostr(i)+' ';
    writeln(s1+s3+s2);
  end;

  close(input);
  close(output);
end.
