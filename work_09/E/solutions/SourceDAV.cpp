#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>

using namespace std;

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);
	
	int a = 0, b = 0, c = 0;

	int i = 0;
	while (1)
	{
		i++;

		int t;
		cin >> t;
		
		if (t == 0)
		{
			break;
		}

		if (t % 2 == 1 && a < t)
		{
			a = t;
		}


		if (t % 2 == 1 && i % 2 == 1)
		{
			if (t > b)
			{
				b = t;
				c = 0;
			}
			if (t == b)
			{
				c++;
			}
		}
	}

	if (b != 0)
	{
		cout << b << ' ' << c;
	}
	else
	{
		cout << a << ' ' << -1;
	}

	return 0;
}