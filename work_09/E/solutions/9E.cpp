#include <fstream>

bool odd(int a){ return a % 2 == 1; }

int main()
{
	int a = 2, max = -1, maxodd = -1, num_maxodd = 0, pos = 0;
	std::ifstream in("input.txt");
	std::ofstream out("output.txt");

	while (a>0) {
		++pos;
		in >> a;
		if (odd(a) && a > max) max = a;
		if (odd(a) && odd(pos))
			if (a > maxodd) {
				maxodd = a;
				num_maxodd = 1;
			}
			else if (a == maxodd) 
				++num_maxodd;
	}
	if (num_maxodd > 0) out << maxodd << " " << num_maxodd;
		else out << max << " -1";
	out.close();
	return 0;
}
