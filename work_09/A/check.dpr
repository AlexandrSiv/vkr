{$APPTYPE CONSOLE}
uses testlib, sysutils;
var answer,user,delta: double;
Begin
   answer := ans.ReadReal;
   user := ouf.ReadReal;
   delta:= abs(answer - user);
   if delta > 1E-3 then QUIT (_WA, 'Difference should be less than 0.001, actually '+FloatToStr(delta));
   QUIT ( _OK, 'Ok' );
End.