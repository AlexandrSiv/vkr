#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

using namespace std;

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	double x;

	cin >> x;

	if (x > 1.0)
	{
		cout << 1;
		return 0;
	}

	if (x < -1.0)
	{
		cout << 0;
		return 0;
	}

	double s = 0;

	if (x > 0)
	{
		s = 1.0 - (1 - x) * (1 - x) / 2.0;
	}
	else
	{
		s = 1.0 - (1 + x) * (1 + x) / 2.0;
	}

	printf("%.4lf", s);

	return 0;
}