#include <fstream>
#include <iostream>

int main(int argc, char* argv[])
{
	std::ifstream in("input.txt");
	std::ofstream out("output.txt");

	double S, x0;
	in >> x0;

	out.precision(4);
	if (x0 <= -1) {
		S = 0;
 	} else if (x0>1) {
                S = 1;
	} else {
		S = x0 >= 0 ? 1 - (1 - x0)*(1 - x0) / 2 : (1 + x0)*(1 + x0) / 2;
	}

	out << std::fixed << S << "\n";
	out.close();
}

