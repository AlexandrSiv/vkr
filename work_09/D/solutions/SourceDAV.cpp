#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>

using namespace std;

int main()
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	int a[1010];
	int b[1010];
	for (int i = 0; i < 1010; i++)
	{
		a[i] = 0;
	}

	int n;

	cin >> n;

	for (int i = 0; i < n; i++)
	{
		int tmp;
		cin >> tmp;
		a[tmp]++;
	}

	if (n % 2 == 1)
	{
		bool fl = true;

		for (int i = 0; i < 1010; i++)
		{
			if (a[i] % 2 == 1)
			{
				fl = false;
				b[n / 2] = i;
				a[i]--;
				break;
			}
		}

		if (fl)
		{
			cout << "No";
			return 0;
		}
	}

	int l = 0;

	for (int i = 1009; i >= 0; i--)
	{
		if (a[i] % 2 == 1)
		{
			cout << "No";
			return 0;
		}
		while (a[i])
		{
			a[i] -= 2;
			b[l] = i;
			b[n - 1 - l] = i;
			l++;
		}
	}

	for (int i = 0; i < n; i++)
	{
		cout << b[i] << ' ';
	}

	return 0;
}