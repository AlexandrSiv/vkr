#include "testlib.h"
#include <stdio.h>
#include <string>

int main(int argc, char * argv[])
{
    setName("balloons checker");
    registerTestlibCmd(argc, argv);

    std::string ans_word = upperCase(ans.readWord());
    if (ans_word=="NO") {
      std::string ouf_word = upperCase(ouf.readWord());
      if (ouf_word=="NO") {
          quitf(_ok, "ok!");
      } else {
        quitf(_wa, "No ???");
      }
    }
    int n = inf.readInt();
    int i, k1, k2;
    int a[1001], b[1001], c[1001];
    for (i=1; i<=1000; i++) {
      a[i]=0;
      b[i]=0;
    }
    for (i=0; i<n; i++) {
      k1 = ouf.readInt();
      k2 = inf.readInt();
      if (k1<1 || k1>1000 || k2<1 || k2>1000) quitf(_wa, "some number not between 1 and 1000");
      c[i] = k1;
      a[k1]++;
      b[k2]++;
    }
    for (i=1; i<=1000; i++) {
      if (a[i]!=b[i]) quitf(_wa, "series of numbers not same, %d, %d, %d", i, a[i], b[i]);
    }
    for (i=0; i < (n/2); i++) {
      if (c[i]!=c[n-i-1]) quitf(_wa, "no symmetry in output %d %d", c[i], c[n-i-1]);
    }

    quitf(_ok, "ok!");
}
