# -*- coding: utf-8 -*=
from datetime import datetime
from functools import reduce
from os import listdir
from os.path import isdir, isfile, join
from flask import abort, session, request, redirect, render_template, url_for
from multimeter.helpers import load_json, save_json, validate_code, admin_required

from database import db_session
from models import WorkTask


class Tasks:
    def __init__(self, settings, languages):
        self._settings = settings
        self._languages = languages
        self.tasks = dict()
        self.load()

    def __len__(self):
        return len(self.tasks)

    def __setitem__(self, key, value):
        self.tasks[key] = value

    def __getitem__(self, item):
        return self.tasks[item]

    def __contains__(self, item):
        return item in self.tasks

    def __iter__(self):
        return self.tasks.__iter__()

    def items(self):
        return self.tasks.items()

    def keys(self):
        return sorted(self.tasks.keys())

    def load(self):
        for name in listdir(self._settings.work_dir):
            path = join(self._settings.work_dir, name)
            if isdir(path) and '.' not in path:
                try:
                    self.tasks[name] = Task(path)
                except (TypeError, FileNotFoundError, UnicodeDecodeError):
                    pass

    def save(self):
        for task in self.tasks:
            task.save()

    def get_results(self, task_code, username, attempt=None):
        answer = []
        results_dir = join(self._settings.work_dir, '.results')
        for filename in listdir(results_dir):
            if filename[-5:] != '.json':
                continue
            name = filename[:-5]
            (_task_code, _username, _attempt) = name.split('-')
            if task_code != _task_code:
                continue
            if username != _username:
                continue
            if attempt is not None and attempt != _attempt:
                continue
            res = load_json(filename, {}, results_dir)
            res['attempt'] = int(_attempt)
            answer.append(res)
        return sorted(answer, key=lambda x: x['attempt'])

    def task_view(self, code):
        """ Страница задачи
        :param code: код задачи
        """
        #if code not in self.tasks:
        #    abort(404)

        object = WorkTask.query.filter_by(code=code).first()
        objects = WorkTask.query.all()
        if object is not None:
            object.code
        else:
            abort(404)
        #task = self.tasks[code]
        task = Task('')
        task.code = object.code
        task.name = object.name
        task.text = object.text
        user_result = self.get_results(code, session['username'])
        return render_template('task.html', code=code, tasks=objects, task=task, languages=self._languages, results=user_result)

    def task_results(self, code, attempt):
        """ Результаты попытки
        :param code: Код задания
        :param attempt: Номер попытки
        :return: Веб-страница
        """
        username = session['username']
        user_result = self.get_results(code, username, attempt)[0]
        task = self.tasks[code]
        total = 0
        totals = {}
        for key, values in user_result['results'].items():
            test_suite = task['test_suites'][key]
            if test_suite['scoring'] == 'entire':
                totals[key] = reduce(lambda a, b: a if b['result'] == 'OK' else 0, values, test_suite['total_score'])
            elif test_suite['scoring'] == 'partial':
                totals[key] = reduce(lambda a, b: a + test_suite['test_score'] if b['result'] == 'OK' else a, values, 0)
            total += totals.get(key, 0)
        return render_template('task_results.html', code=code, attempt=attempt, results=user_result, totals=totals,
                               total=total)

    def task_submission(self, code):
        """ Страница обработки полученного решения
        :param code: Код задачи
        """
        if code not in self.tasks:
            abort(404)
        if datetime.now() > self._settings.end_time:
            return render_template('game_over.html', code=code)
        lang_code = request.form.get('language')
        if lang_code not in self._languages:
            abort(404)
        username = session['username']
        session['last_language'] = lang_code
        user_result = self.get_results(code, username)
        attempt = str(len(user_result) + 1).strip()

        attempt_filename = "{}-{}-{}-{}".format(code, username, lang_code, attempt)
        attempt_file = request.files['file']
        attempt_file.save(join(self._settings.work_dir, '.queue', attempt_filename))

        # пустой файл результатов создается тут, а не в арбитре, чтобы
        # не перезаписать его в случае, если пользователь отправит файл снова
        # до того, как предыдущая попытка будет проверена
        results_filename = '{}-{}-{}.json'.format(code, username, attempt)
        save_json({}, results_filename, join(self._settings.work_dir, '.results'))

        return render_template('waiting.html', code=code)

    @admin_required
    def admin_tasks(self):
        """ Список заданий """
        if request.method == 'POST':
            code = request.form.get('code', '')
            if code in self.tasks:
                del (self.tasks[code])
                task = WorkTask.query.filter_by(code=code).first()
                if task is not None:
                    db_session.delete(task)
                    db_session.commit()
                #self.save()
        objects = WorkTask.query.all()
        workTasks = dict()
        for object in objects:
            workTasks[object.code] = dict(
                config_filename='',
                statement='',
                statement_filename='',
                task_dir='',
                test_suites={},
                preliminary=[],
                name=object.name,
                brief_name=object.brief_name,
                input_file=object.input_file,
                output_file=object.output_file,
                timeout=object.timeout,
                text=object.text
            )
            self.tasks[object.code] = dict(
                config_filename='',
                statement='',
                statement_filename='',
                task_dir='',
                test_suites={},
                preliminary=[],
                name=object.name,
                brief_name=object.brief_name,
                input_file=object.input_file,
                output_file=object.output_file,
                timeout=object.timeout,
                text=object.text
            )
        #return render_template('admin_tasks.html', codes=sorted(self.tasks.keys()))
        return render_template('admin_tasks.html', codes=sorted(workTasks))

    def validate_task(self, code, data, check_uniqueness):
        """ Проверка задания
        :param check_uniqueness:
        :param data:
        :param code:
        """
        codes_list = self.tasks if check_uniqueness else []
        errors = validate_code(code, codes_list, 'Код задания')
        if not data.get('name'):
            errors.append('Наименование не задано')
        return errors

    @admin_required
    def admin_task(self, code=None):
        """ Страница редактирования задания
        :param code: Код задачи
        """
        errors = []
        if code is None:
            new_code = ''
            new_data = {'tests': {}}
            new_data = dict(test_suites={})
        else:
            if code not in self.tasks:
                abort(404)
            new_code = code
            new_data = self.tasks[code]
        if request.method == 'POST':
            new_code = request.form.get('code', '')
            new_data = dict(
                name=request.form.get('name', ''),
                brief_name=request.form.get('brief_name', ''),
                statement=request.form.get('statement', ''),
                test_suites={},
            )
            # for key, value in request.form.items():
            #     if key[:8] == 'filename':
            #         new_data['tests'][value] = int(request.form['score' + key[8:]])
            errors = self.validate_task(new_code, new_data, code != new_code)
            if not errors:
                #self.tasks[new_code] = new_data
                #if code is not None and code != new_code:
                #    del (self.tasks[code])
                #save_json(self.tasks, join(self._settings.work_dir, new_code + '/task.json'))
                code = request.form.get('code', '')
                name = request.form.get('name', '')
                brief_name = request.form.get('brief_name', '')
                input_file = request.form.get('input_file', '')
                output_file = request.form.get('output_file', '')
                timeout = request.form.get('timeout', '')
                text = request.form.get('text', '')
                wt = WorkTask(code, name, brief_name, input_file, output_file, timeout, text)
                db_session.add(wt)
                db_session.commit()
                return redirect(url_for('admin_tasks'))
        return render_template('admin_task.html', errors=errors, code=new_code, data=new_data)


class Task:
    def __init__(self, task_dir):
        self.task_dir = task_dir
        self.config_filename = join(task_dir, 'task.json')
        self.statement_filename = join(task_dir, 'task.html')

        self.name = ''
        self.brief_name = ''
        self.timeout = 1.0
        self.input_file = 'input.txt'
        self.output_file = 'output.txt'

        self.statement = ''
        self.preliminary = list()
        self.test_suites = dict()

        #self.load()

    def __getitem__(self, item):
        return self.__dict__.__getitem__(item)

    def __setitem__(self, key, value):
        self.__dict__.__setitem__(key, value)

    def load(self):
        try:
            statement = open(self.statement_filename, encoding='utf-8')
            self.statement = statement.read()
        except FileNotFoundError:
            pass

        keys = ['name', 'brief_name', 'timeout', 'input_file', 'output_file', 'test_suites']
        config_file = load_json(self.config_filename, {})
        self.__dict__.update((k, v) for k, v in config_file.items() if k in keys)

        preliminary_dir = join(self.task_dir, 'preliminary')
        names = [name for name in listdir(preliminary_dir) if '.' not in name]
        for name in names:
            input_file = join(preliminary_dir, name)
            answer_file = join(preliminary_dir, name + '.a')
            if isfile(input_file) and isfile(answer_file):
                self.preliminary.append(name)

        for test_suite in self.test_suites:
            self.test_suites[test_suite]['tests'] = []
            tests_dir = join(self.task_dir, 'tests', test_suite)
            names = [name for name in listdir(tests_dir) if '.' not in name]
            for name in names:
                input_file = join(tests_dir, name)
                answer_file = join(tests_dir, name + '.a')
                if isfile(input_file) and isfile(answer_file):
                    self.test_suites[test_suite]['tests'].append(name)

    def save(self):
        keys = ['name', 'brief_name', 'timeout', 'input_file', 'output_file', 'test_suites']
        config = dict(zip(keys, [self.__dict__[k] for k in keys]))
        save_json(config, self.config_filename)

        with open(self.statement_filename, mode='w', encoding='utf-8') as f:
            f.write(self.statement)
            f.close()
