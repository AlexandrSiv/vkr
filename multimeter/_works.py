from flask import render_template, abort
from models import Work
from multimeter.helpers import admin_required


@admin_required
def admin_works():
    works = Work.query.all()
    return render_template('admin_works.html', works=works)


@admin_required
def admin_work(id=None):
    if id is None:
        abort(404)
    else:
        work = Work.query.get(id)
    return render_template('admin_work.html', work=work)
