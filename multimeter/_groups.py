from flask import render_template, abort

from models import Group
from multimeter import admin_required


@admin_required
def admin_groups():
    groups = Group.query.all()
    return render_template('admin_groups.html', groups=groups)


@admin_required
def admin_group(id=None):
    if id is None:
        abort(404)
    group = Group.query.get(id)
    return render_template('admin_group.html', group=group)