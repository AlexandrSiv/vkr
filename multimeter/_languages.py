# -*- coding: utf-8 -*=

"""
Языки программирования
Languages - контейнер для языков программирования. В отличие от dict он умеет работать с конфигурационным файлом. Кроме
    того, обход элеменов осуществляется с учетом сортировки по представлению языка программирования.
Language - базовый класс для языков программирования
Классы-наследнки Language - классы конкретного языка программирования. Конструктор каждого такого класса должен уметь
    определять местонахождение компилятора и/или интерпретатора. Наименование каждого такого класса должно заканчиваться
    на суффикс Lang
"""
import os
from abc import ABCMeta, abstractmethod
from os.path import isdir, isfile, join
from multimeter.helpers import load_json, save_json, validate_code, check_or_create_dir


class Languages:
    """ Языки программирования """

    def __init__(self, work_dir=None):
        self.directory = join(work_dir, '.languages')
        self.languages = dict()
        self.filename = join(work_dir, 'languages.json')

        if work_dir:
            self.load(work_dir)

    def load(self, work_dir):
        """ Чтение языков программирования из конфигурационного файла
        :param work_dir: Абсолюбтный путь к рабочему каталогу
        """
        check_or_create_dir(self.directory)
        for filename in os.listdir(self.directory):
            os.remove(join(self.directory, filename))

        self.filename = join(work_dir, 'languages.json')
        data_from_file = load_json(self.filename, {})
        known_classes = [JavaLang, VisualCppLang, VisualBasicLang, VisualCSharpLang, FreePascalLang,
                         PascalABCLang, BorlandDelphi7Lang, Python2Lang, Python3Lang, GccCppLang]
        for class_name, language_data in data_from_file.items():
            cls = Language
            for cur_class in known_classes:
                if class_name == cur_class.__name__:
                    cls = cur_class
            try:
                new_language = cls(class_name, self.directory, language_data)
                self.languages[class_name] = new_language
            except (AttributeError, TypeError, Exception):
                pass

    def autodiscover(self):
        """ Автоматическое определение языков программирования """
        self.languages.clear()
        known_classes = [JavaLang, VisualCppLang, VisualBasicLang, VisualCSharpLang, FreePascalLang, PascalABCLang,
                         BorlandDelphi7Lang, Python2Lang, Python3Lang, GccCppLang]
        for class_name in known_classes:
            try:
                new_language = class_name(class_name.__name__, self.directory)
            except (AttributeError, TypeError, Exception):
                new_language = None
            if new_language is not None:
                self.languages[class_name.__name__] = new_language

    def save(self):
        """ Сохранение языков программирования в конфигурационный файл """
        prepared_data = {}
        for class_name, language in self.languages.items():
            prepared_data[class_name] = language.__dict__
        save_json(prepared_data, self.filename)

    def validate(self, code, data, check_uniqueness):
        codes_list = self.languages.keys() if check_uniqueness else []
        errors = validate_code(code, codes_list, 'Код')
        if data.get('name', '') == '':
            errors.append('Наименование не задано')
        return errors

    def verifying(self):
        """ Проверка языков программирования """
        answer = True
        for lang in self.languages.values():
            answer = answer and lang.verify()
        return answer

    def __iter__(self):
        return iter(sorted(self.languages.keys(), key=lambda class_name: str(self.languages[class_name])))

    def __setitem__(self, code, data):
        if isinstance(data, Language):
            self.languages[code] = data
        if isinstance(data, dict):
            self.languages[code] = Language(code, self.directory, data)

    def __getitem__(self, code):
        return self.languages[code]

    def __delitem__(self, code):
        del self.languages[code]

    def __contains__(self, code):
        return code in self.languages


class Language:
    """ Базовый класс для языка программирования """

    def __init__(self, code, directory, data):
        self.name = data['name']
        self.extension = data['extension']
        self.execution = data['execution']
        self.compilation = data['compilation']

        if not self.verify():
            raise Exception('{} verification failed !!!'.format(self.name))

        self.batch = join(directory, code + '.bat')
        batch_file = open(self.batch, "w")
        for command in self.compilation:
            batch_file.write(command + '\n')
        batch_file.close()

    def __repr__(self):
        return self.name

    def verify(self):
        return True


class JavaLang(Language):
    def __init__(self, code, directory, data=None):
        self.version = ''
        self.java_home = ''
        self.jvm = ''
        self.compiler = ''

        if data is None:
            paths = ['C:\\Program Files (x86)\\Java', 'C:\\Program Files\\Java']
            for path in paths:
                if not isdir(path):
                    continue
                jdk_list = [jdk for jdk in os.listdir(path) if jdk.lower().startswith('jdk') and isdir(join(path, jdk))]
                if len(jdk_list) == 0:
                    continue
                jdk = sorted(jdk_list)[-1]
                java_home = join(path, jdk)
                jvm = join(java_home, 'bin', 'java.exe')
                compiler = join(java_home, 'bin', 'javac.exe')
                if isdir(java_home) and isfile(jvm) and isfile(compiler):
                    self.version = jdk[3:]
                    self.java_home = java_home
                    self.jvm = jvm
                    self.compiler = compiler
        else:
            self.version = data.get('version', '')
            self.java_home = data.get('java_home', '')
            self.jvm = data.get('jvm', '')
            self.compiler = data.get('compiler', '')

        super().__init__(code, directory, dict(
            name='Java ' + self.version,
            extension='java',
            execution='"' + self.jvm + '" Main',
            compilation=['"' + self.compiler + '" %1.java'],
        ))

    def verify(self):
        return self.version != '' and isdir(self.java_home) and isfile(self.jvm) and isfile(self.compiler)


class VisualStudio(Language, metaclass=ABCMeta):
    @classmethod
    @abstractmethod
    def get_name(cls):
        pass

    @classmethod
    @abstractmethod
    def get_extension(cls):
        pass

    @abstractmethod
    def get_compiler(self, path, version):
        pass

    @abstractmethod
    def get_compilation(self):
        pass

    def __init__(self, code, directory, data=None):
        self.version = ''
        self.prepare = ''
        self.compiler = ''

        if data is None:
            paths = ['C:\\Program Files (x86)', 'C:\\Program Files']
            versions = {
                '2008': '9.0',
                '2010': '10.0',
                '2012': '11.0',
                '2013': '12.0',
                '2015': '14.0',
            }
            name = self.get_name()
            for path in paths:
                for year, version in versions.items():
                    prepare = join(path, 'Microsoft Visual Studio ' + version, 'Common7', 'Tools', 'vsvars32.bat')
                    compiler = self.get_compiler(path, version)
                    if isfile(prepare) and isfile(compiler):
                        self.version = version
                        self.prepare = prepare
                        self.compiler = compiler
                        name = name + ' ' + year
        else:
            self.version = data.get('version', '')
            self.prepare = data.get('prepare', '')
            self.compiler = data.get('compiler', '')
            name = data.get('name', '')

        super().__init__(code, directory, dict(
            name=name,
            extension=self.get_extension(),
            execution='{solution}.exe',
            compilation=self.get_compilation(),
        ))


class VisualCppLang(VisualStudio):
    def verify(self):
        return self.version != '' and isfile(self.prepare) and isfile(self.compiler)

    def get_compiler(self, path, version):
        return join(path, 'Microsoft Visual Studio ' + version, 'VC', 'bin', 'cl.exe')

    def get_compilation(self):
        return ['call "' + self.prepare + '"', 'cl %1.cpp']

    @classmethod
    def get_name(cls):
        return 'MS Visual C++'

    @classmethod
    def get_extension(cls):
        return 'cpp'


class VisualCSharpLang(VisualStudio):
    def verify(self):
        return self.version != '' and isfile(self.prepare) and isfile(self.compiler)

    def get_compiler(self, path, version):
        filename = 'csc.exe'
        compiler = join(path, 'MSBuild', version, 'Bin', filename)
        if not isfile(compiler):
            framework_dir = join(os.environ.get('WINDIR'), 'Microsoft.NET')
            platforms = [p for p in ['Framework', 'Framework64'] if isdir(join(framework_dir, p))]
            for platform in platforms:
                platform_dir = join(framework_dir, platform)
                versions = [v for v in os.listdir(platform_dir) if isdir(join(platform_dir, v))]
                for version in sorted(versions):
                    version_dir = join(platform_dir, version)
                    candidate =  join(version_dir, filename)
                    if isfile(candidate):
                        compiler = candidate
        return compiler

    def get_compilation(self):
        return ['call "' + self.prepare + '"', 'csc %1.cs']

    @classmethod
    def get_name(cls):
        return 'MS Visual C#'

    @classmethod
    def get_extension(cls):
        return 'cs'


class VisualBasicLang(VisualStudio):
    def verify(self):
        return self.version != '' and isfile(self.prepare) and isfile(self.compiler)

    def get_compiler(self, path, version):
        filename = 'vbc.exe'
        compiler = join(path, 'MSBuild', version, 'Bin', filename)
        if not isfile(compiler):
            framework_dir = join(os.environ.get('WINDIR'), 'Microsoft.NET')
            platforms = [p for p in ['Framework', 'Framework64'] if isdir(join(framework_dir, p))]
            for platform in platforms:
                platform_dir = join(framework_dir, platform)
                versions = [v for v in os.listdir(platform_dir) if isdir(join(platform_dir, v))]
                for version in sorted(versions):
                    version_dir = join(platform_dir, version)
                    candidate =  join(version_dir, filename)
                    if isfile(candidate):
                        compiler = candidate
        return compiler

    def get_compilation(self):
        return ['call "' + self.prepare + '"', 'vbc %1.vb']

    @classmethod
    def get_name(cls):
        return 'MS Visual Basic'

    @classmethod
    def get_extension(cls):
        return 'vb'


class FreePascalLang(Language):
    def verify(self):
        return self.version != '' and isfile(self.compiler)

    def __init__(self, code, directory, data=None):
        self.version = ''
        self.compiler = ''

        if data is None:
            versions = ['2.2.2', '2.2.4', '2.4.0', '2.4.2', '2.4.4', '2.6.0', '2.6.2', '2.6.4']
            platforms = ['i386-win32', 'x86_64-win64']
            for version in versions:
                for platform in platforms:
                    compiler = join('C:\\FPC', version, 'bin', platform, 'fpc.exe')
                    if isfile(compiler):
                        self.version = version
                        self.compiler = compiler
        else:
            self.version = data.get('version', '')
            self.compiler = data.get('compiler', '')

        super().__init__(code, directory, dict(
            name='Free Pascal ' + self.version,
            extension='pas',
            execution='{solution}.exe',
            compilation=[self.compiler + ' %1.pas'],
        ))


class PascalABCLang(Language):
    def verify(self):
        return isfile(self.compiler)

    def __init__(self, code, directory, data=None):
        self.compiler = ''

        if data is None:
            paths = ['C:\\Program Files (x86)', 'C:\\Program Files']
            for path in paths:
                compiler = join(path, 'PascalABC.NET', 'pabcnetcclear.exe')
                if isfile(compiler):
                    self.compiler = compiler
        else:
            self.compiler = data.get('compiler', '')

        super().__init__(code, directory, dict(
            name='PascalABC.NET',
            extension='pas',
            execution='{solution}.exe',
            compilation=['"' + self.compiler + '" %1.pas'],
        ))


class BorlandDelphi7Lang(Language):
    def verify(self):
        return isfile(self.compiler)

    def __init__(self, code, directory, data=None):
        self.compiler = ''

        if data is None:
            paths = ['C:\\Program Files (x86)', 'C:\\Program Files']
            for path in paths:
                compiler = join(path, 'Borland', 'Delphi7', 'Bin', 'DCC32.exe')
                if isfile(compiler):
                    self.compiler = compiler
        else:
            self.compiler = data.get('compiler', '')

        super().__init__(code, directory, dict(
            name='Borland Delphi 7',
            extension='dpr',
            execution='{solution}.exe',
            compilation=['"' + self.compiler + '" %1.dpr'],
        ))


class Python2Lang(Language):
    def verify(self):
        return self.version != '' and isfile(self.interpreter)

    def __init__(self, code, directory, data=None):
        self.version = ''
        self.interpreter = ''

        if data is None:
            versions = ['2.6', '2.7']
            for version in versions:
                interpreter = join('C:\\Python' + version.replace('.', ''), 'python.exe')
                if isfile(interpreter):
                    self.version = version
                    self.interpreter = interpreter
        else:
            self.version = data.get('version', '')
            self.interpreter = data.get('interpreter', '')

        super().__init__(code, directory, dict(
            name='Python ' + self.version,
            extension='py',
            execution=self.interpreter + ' {solution}.py',
            compilation=['@echo Compilation %1.py'],
        ))


class Python3Lang(Language):
    def verify(self):
        return self.version != '' and isfile(self.interpreter)

    def __init__(self, code, directory, data=None):
        self.version = ''
        self.interpreter = ''

        if data is None:
            versions = ['3.2', '3.3', '3.4']
            for version in versions:
                interpreter = join('C:\\Python' + version.replace('.', ''), 'python.exe')
                if isfile(interpreter):
                    self.version = version
                    self.interpreter = interpreter

            # versions = ['3.5']
            # platforms = ['32', '64']
            # programs_path = join(os.environ['USERPROFILE'], 'Local Settings', 'Application Data', 'Programs')
            # if isdir(programs_path):
            #     for version in versions:
            #         for platform in platforms:
            #             suffix = version.replace('.', '') + '-' + platform
            #             interpreter = join(programs_path, 'Python' + suffix, 'python.exe')
            #             if isfile(interpreter):
            #                 self.version = version
            #                 self.interpreter = interpreter
        else:
            self.version = data.get('version', '')
            self.interpreter = data.get('interpreter', '')

        super().__init__(code, directory, dict(
            name='Python ' + self.version,
            extension='py',
            execution=self.interpreter + ' {solution}.py',
            compilation=['@echo Compilation %1.py'],
        ))


class GccCppLang(Language):
    def verify(self):
        return isfile(self.compiler)

    def __init__(self, code, directory, data=None):
        self.compiler = ''

        if data is None:
            paths = ['C:\\Program Files (x86)\\CodeBlocks', 'C:\\Program Files\\CodeBlocks', 'C:']
            for path in paths:
                compiler = join(path, 'MinGW', 'bin', 'g++.exe')
                if isfile(compiler):
                    self.compiler = compiler
        else:
            self.compiler = data.get('compiler', '')

        super().__init__(code, directory, dict(
            name='GNU Compiler Collection C++',
            extension='cpp',
            execution='{solution}.exe',
            compilation=['"' + self.compiler + '" %1.cpp -o %1'],
        ))
