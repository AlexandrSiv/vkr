# -*- coding: utf-8 -*=
from ._languages import Languages
from ._results import Results
from ._settings import Settings
from ._tasks import Tasks
from ._users import Users
from .helpers import load_json, results_dir, moment, queue_dir, save_json, validate_code, admin_required

settings = Settings()
languages = Languages(settings.work_dir)
tasks = Tasks(settings, languages)
users = Users(settings)
results = Results(results_dir(settings), tasks, users, settings)
