# -*- coding: utf-8 -*-
import os
from collections import Sized

from flask import session, redirect, url_for, abort, request, render_template
from flask_babelex import gettext
from database import db_session

from models import User
from multimeter.helpers import load_json, save_json, validate_code, admin_required


class Users(Sized):
    def __init__(self, settings):
        self._settings = settings
        self.filename = os.path.join(settings.work_dir, 'users.json')
        self.users = dict()
        self.load()

    def __len__(self):
        return self.users.__len__()

    def __setitem__(self, key, value):
        self.users.__setitem__(key, value)

    def __getitem__(self, item):
        return self.users.__getitem__(item)

    def __contains__(self, item):
        return self.users.__contains__(item)

    def __iter__(self):
        return self.users.__iter__()

    def clear(self):
        self.users.clear()

    def load(self):
        self.users = load_json(self.filename, {})

    def save(self):
        save_json(self.users, self.filename)

    def validate_user(self, username, data, confirm, check_uniqueness):
        """ Проверка пользователя
        :param username: Имя пользователя
        :param data: Данные пользователя
        :param confirm: Подтверждение пароля пользователя
        :param check_uniqueness: Нужно ли проверять уникальность имени пользователя
        """
        # TODO: Нужно добавить проверку наличия обязательных полей пользователя (фамилия, имя, дата рождения и т.д.)
        users_list = self.users if check_uniqueness else []
        errors = validate_code(username, users_list, 'Логин')
        password = data.get('password', '')
        if password == '':
            errors.append(gettext('Password not specified'))
        if len(password) > 20:
            errors.append(gettext('Password is longer than 20 characters'))
        if password != confirm:
            errors.append(gettext('Passwords do not match'))
        return errors

    def register(self):
        """ Регистрация нового пользователя """
        if not self._settings.registration_enabled:
            abort(404)
        errors = []
        new_name = ''
        new_data = {}
        if request.method == 'POST':
            new_name = request.form.get('username', '')
            new_data = dict(password=request.form.get('password', ''))
            for attr in self._settings.users_attributes:
                new_data[attr] = request.form.get(attr, '')
            errors = self.validate_user(new_name, new_data, request.form.get('confirm', ''), True)
            if not errors:
                self.users[new_name] = new_data
                self.save()
                session['username'] = new_name
                return redirect(url_for('index'))
        return render_template('register.html', errors=errors, username=new_name, data=new_data)

    @admin_required
    def admin_user(self, username=None):
        """ Редактирование пользователя администратором
        :param username: Имя пользователя
        """
        errors = []
        if username is None:
            new_name = ''
            new_data = {}
        else:
            if username not in self.users:
                abort(404)
            new_name = username
            new_data = self.users[username]
        if request.method == 'POST':
            new_name = request.form.get('username', '')
            new_data = dict(password=request.form.get('password', ''))
            for attr in self._settings.users_attributes:
                new_data[attr] = request.form.get(attr, '')
            errors = self.validate_user(new_name, new_data, new_data['password'], username != new_name)
            if not errors:
                new_user = User(username, new_data['first_name'], new_data['second_name'], new_data['last_name'],
                      new_data['password'], new_data['class'], new_data['school'], new_data['birthday'], new_data['teacher'])
                db_session.add(new_user)
                db_session.commit()
                self.users[new_name] = new_data
                if username is not None and username != new_name:
                    del (self.users[username])
                self.save()
                return redirect(url_for('admin_users'))
        return render_template('admin_user.html', errors=errors, username=new_name, data=new_data)

    def login(self):
        """ Страница авторизации пользователей """
        if 'username' in session:
            return redirect(request.form.get('next', url_for('index')))
        errors = []
        if request.method == 'POST':
            username = request.form.get('username', '')
            password = request.form.get('password', '')
            if username == '':
                errors.append(gettext('Login not specified'))
            if password == '':
                errors.append(gettext('Password not specified'))
            if not errors:
                user = User.query.filter_by(login=username, password=password).first()
                if user is not None:
                    session['username'] = username
                    return redirect(request.form.get('next', url_for('main')))
                errors.append(gettext('Wrong login or password'))
        return render_template('login.html', errors=errors)

    def admin_login(self):
        """ Страница авторизации администратора системы """
        if 'admin' in session:
            return redirect(request.form.get('next', url_for('admin_index')))
        message = None
        if request.method == 'POST':
            if request.form['password'] == self._settings.password:
                session['admin'] = True
                return redirect(request.form.get('next', url_for('admin_index')))
            else:
                message = 'Неправильный пароль'
        return render_template('admin_login.html', message=message)

    @staticmethod
    def logout():
        """ Выход из системы """
        session.pop('username', None)
        return redirect(url_for('main'))

    @staticmethod
    def admin_logout():
        """ Выход администратора из системы """
        session.pop('admin', None)
        return redirect(url_for('main'))

    @admin_required
    def admin_users(self):
        """ Список пользователей системы """
        if request.method == 'POST':
            username = request.form.get('username', '')
            if username in self.users:
                del (self.users[username])
                self.save()
        return render_template('admin_users.html', usernames=sorted(self.users.keys()))
