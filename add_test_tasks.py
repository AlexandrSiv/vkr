__author__ = 'AlexanderSivtsev'
# -*- coding: utf-8 -*-
from server.database import db_session
from server.models import Task

task = Task(u"Для чего используется директива #include в C/C++? Пример, #include <iostream>", None, 2,
            u"Объявление что заданный файл исходный", u"Подключение указанного исходного файла к текущему",
            u"Удаление(игнорирование) указанного файла", u"#include не ялвяется директивой компилятора")
db_session.add(task)

task = Task(u"Что выведет данный кусок кода?", None, 4, u"2", u"4", u"6", u"ни один из предыдущих ответов неправильный")
db_session.add(task)

task = Task(u"Оператор для динамического выделения памяти", None, 3, u"sizeof()", u"delete", u"new", u"create")
db_session.add(task)

task = Task(u"Какие параметры не принимает конструктор вектора в C/C++", None, 3, u"vector()", u"vector(10, ' ')",
            u"vector(3, 4, 5)", u"vector(3)")
db_session.add(task)

task = Task(u"Что делает метод pop() вектора в C/C++", None, 4, u"Добавление в начало", u"Удаление массива",
            u"Создание массива", u"Удаление с конца")
db_session.add(task)

db_session.commit()