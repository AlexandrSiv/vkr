﻿# -*- coding: utf-8 -*-
import os
import socket
import sys
from datetime import datetime
from functools import reduce

from flask import Flask
from flask import render_template, abort, session, request, url_for, redirect
from flask_babelex import Babel
from flask_seasurf import SeaSurf

from database import init_db
from models import WorkTask, Result
from multimeter import load_json, results_dir, admin_required, moment
from multimeter import users, languages, settings, results, tasks
from multimeter._groups import admin_group, admin_groups
from multimeter._works import admin_work, admin_works

# Настройка Flask
app = Flask(__name__)
init_db()
app.config['SECRET_KEY'] = 'b3ae4cfc3de6b0f6be6f82c751e0175554f47a5b61ee5f2be7fc1fd0b312bf71'
app.config['DEBUG'] = app.config['TESTING'] = settings.development
app.config['PORT'] = settings.port
if not os.path.isdir(settings.work_dir):
    print("Work folder", settings.work_dir, "not found !!!")
    sys.exit()

# Настройка плагинов
sea_surf = SeaSurf(app)
babel = Babel(app)
app.config['BABEL_DEFAULT_LOCALE'] = 'ru'
app.config['BABEL_DEFAULT_TIMEZONE'] = 'UTC'


@babel.localeselector
def get_locale():
    if 'username' in session:
        username = session['username']
        if username in users:
            user = users[username]
            if 'locale' in user:
                return user['locale']
    return request.accept_languages.best_match(['en', 'ru'])


@babel.timezoneselector
def get_timezone():
    if 'username' in session:
        username = session['username']
        if username in users:
            user = users[username]
            if 'timezone' in user:
                return user['timezone']
    return 'UTC'


@app.context_processor
def multimeter_context():
    return dict(
        locale=get_locale(),
        settings=settings,
        tasks=tasks,
        users=users,
    )


@app.context_processor
def time_left():
    try:
        end_time = settings['end_time']
    except KeyError:
        return dict(time_left="0")
    except ValueError:
        return dict(time_left="0")
    t = end_time - datetime.now()
    return dict(time_left=str(int(max(0, t.total_seconds()) / 60)))


def login_required(f):
    """ Декоратор обязательной авторизации пользователей
    :param f: Функция, генерирующая страницу для авторизированных пользователей
    """
    from functools import wraps

    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'username' not in session or session['username'] not in users:
            next_path = request.path if request.path != url_for('login') else ''
            return redirect(url_for('login', next=next_path))
        return f(*args, **kwargs)

    return decorated_function

app.add_url_rule('/login/', view_func=users.login, methods=('GET', 'POST'))
app.add_url_rule('/logout/', view_func=users.logout)
app.add_url_rule('/register/', view_func=users.register, methods=('GET', 'POST'))

app.add_url_rule('/admin/login/', view_func=users.admin_login, methods=('GET', 'POST'))
app.add_url_rule('/admin/logout/', view_func=users.admin_logout)

app.add_url_rule('/admin/groups/', view_func=admin_groups, methods=('GET', 'POST'))
# app.add_url_rule('/admin/work/', view_func=users.admin_user, methods=('GET', 'POST'), defaults={'username': None})
app.add_url_rule('/admin/group/<id>/', view_func=admin_group, methods=('GET', 'POST'))

app.add_url_rule('/admin/works/', view_func=admin_works, methods=('GET', 'POST'))
# app.add_url_rule('/admin/work/', view_func=users.admin_user, methods=('GET', 'POST'), defaults={'username': None})
app.add_url_rule('/admin/work/<id>/', view_func=admin_work, methods=('GET', 'POST'))

app.add_url_rule('/admin/users/', view_func=users.admin_users, methods=('GET', 'POST'))
app.add_url_rule('/admin/user/', view_func=users.admin_user, methods=('GET', 'POST'), defaults={'username': None})
app.add_url_rule('/admin/user/<username>/', view_func=users.admin_user, methods=('GET', 'POST'))

app.add_url_rule('/task/<code>/', view_func=login_required(tasks.task_view))
app.add_url_rule('/task_results/<code>/<attempt>', view_func=login_required(tasks.task_results))
app.add_url_rule("/task_submission/<code>", view_func=login_required(tasks.task_submission), methods=('POST',))

app.add_url_rule("/admin/tasks/", view_func=tasks.admin_tasks, methods=('GET', 'POST'))
app.add_url_rule("/admin/task/", view_func=tasks.admin_task, methods=('GET', 'POST'))
app.add_url_rule("/admin/task/<code>", view_func=tasks.admin_task, methods=('GET', 'POST'))


# редирект на главную страницу
@app.route('/')
def mainR():
    return redirect('/main')

# путь на главную страницу
@app.route('/main')
def main():
    if session.get('name'):
        return render_template("main.html", session=session)
    else:
        return render_template("main.html")

# путь на страницу о проекте
@app.route('/about')
def about():
    if session.get('name'):
        return render_template("about.html", session=session)
    else:
        return render_template("about.html")

# путь на страницу с контактами разработчика
@app.route('/contacts')
def contacts():
    if session.get('name'):
        return render_template("contacts.html", session=session)
    else:
        return render_template("contacts.html")

# путь получения страницы с определенным уроком
@app.route('/lesson/<level>')
def lesson(level):
    content = {
        "title": u"Урок " + level,
        "tmp_num": level
    }
    return render_template("lesson.html", content=content, session=session)

# обработчик ошибки 404(по ссылке ничего не найдено)
@app.errorhandler(404)
def page_not_found(error):
    content = {
        "title": u"Страница не найдена",
        "status": u"404",
        "message": u"Страница ссылке по которой вы перешли не найдена"
    }
    return render_template('error.html', content=content, session=session), 404

@app.route('/index/')
@login_required
def index():
    """ Главная страница """
    tasks = WorkTask.query.all()
    return render_template('index.html', tasks=tasks)


@app.route('/statement/')
@login_required
def statement():
    if 'statement_file' not in settings:
        abort(404)
    from flask import send_from_directory
    return send_from_directory(directory=settings.work_dir,
                               filename=settings['statement_file'],
                               as_attachment=True)


@app.route('/results/')
@login_required
def get_total_results():
    if not settings.show_results:
        abort(404)

    columns = ['rank', 'name', 'tasks', 'total_maximum_points']
    results.reload()
    worktasks = WorkTask.query.all()
    resultobjects = Result.query.all()
    return render_template('results.html', worktasks=worktasks, results=resultobjects,
                           rows=results.frozen_rows, cells=results.frozen_cells,
                           columns=columns)


def set_config_param(param, value):
    """ Установка параметров конфигурации системы
    :param value:
    :param param:
    """
    settings[param] = value
    settings.save()
    return '<br>Данные успешно сохранены'


@app.route('/admin/', methods=('GET', 'POST'))
@admin_required
def admin_index():
    """ Главная страница интерфейса администратора """
    message = ''
    if request.method == 'POST':
        if 'title' in request.form:  # Изменение заголовка системы
            message += set_config_param('title', request.form['title'])

        if 'statement_file' in request.form:
            message += set_config_param('statement_file', request.form['statement_file'])

        if 'password' in request.form:
            message += set_config_param('password', request.form['password'])

        if 'start_time' in request.form:
            try:
                message += set_config_param('start_time', moment(request.form['start_time'].replace(' ', 'T')))
            except ValueError:
                message += '<br>Ошибка в формате даты и времени. Правильный формат: ГГГГ-ММ-ДД чч:мм:сс'

        if 'freeze_time' in request.form:
            try:
                message += set_config_param('freeze_time', moment(request.form['freeze_time'].replace(' ', 'T')))
            except ValueError:
                message += '<br>Ошибка в формате даты и времени. Правильный формат: ГГГГ-ММ-ДД чч:мм:сс'

        if 'end_time' in request.form:
            try:
                message += set_config_param('end_time', moment(request.form['end_time'].replace(' ', 'T')))
            except ValueError:
                message += '<br>Ошибка в формате даты и времени. Правильный формат: ГГГГ-ММ-ДД чч:мм:сс'

        if 'registration' in request.form:  # Включение / отключение регистрации
            message += set_config_param('registration_enabled', request.form['registration'] == 'enabled')

        if 'show_results' in request.form:  # Включение / отключение отображения общих результатов пользователям
            message += set_config_param('show_results', request.form['show_results'] == 'show')

        if 'testing' in request.form:  # ???
            message += set_config_param('testing', request.form['testing'])

    hostname = socket.gethostname()
    addresses = socket.getaddrinfo(hostname, settings.port)
    addresses = [a[4] for a in addresses if a[0] == socket.AF_INET]
    return render_template('admin_index.html', message=message[4:], addresses=addresses)


@app.route('/admin/languages/', methods=('GET', 'POST'))
@admin_required
def admin_languages():
    """ Список языков программирования """
    if request.method == 'POST':
        action = request.form.get('action', '')
        if action == 'delete':
            code = request.form.get('code', '')
            if code in languages:
                del (languages[code])
                languages.save()
        if action == 'autodiscover':
            languages.autodiscover()
            languages.save()

    return render_template('admin_languages.html', languages=languages)


@app.route('/admin/language/', methods=('GET', 'POST'))
@app.route('/admin/language/<code>/', methods=('GET', 'POST'))
@admin_required
def admin_language(code=''):
    """ Страница редактирования языка программирования
    :param code:
    """
    errors = []
    if code == '':
        cur_data = {}
    elif code not in languages:
        cur_data = {}
        abort(404)
    else:
        cur_data = languages[code]
    if request.method == 'POST':
        new_code = request.form.get('code', '')
        new_data = dict(
            name=request.form.get('name', ''),
            extension=request.form.get('extension', ''),
            execution=request.form.get('execution', ''),
            compilation=request.form.get('compilation', '').splitlines(),
        )
        errors = languages.validate(new_code, new_data, code != new_code)
        if not errors:
            languages[new_code] = new_data
            if code != '' and code != new_code:
                del (languages[code])
            languages.save()
            return redirect(url_for('admin_languages'))
    return render_template('admin_language.html', code=code, data=cur_data, errors=errors)


@app.route('/admin/results/')
@admin_required
def admin_results():
    """ Страница общих результатов """
    columns = ['rank', 'name', 'tasks', 'total_maximum_points']
    results.reload()
    worktasks = WorkTask.query.all()
    resultobjects = Result.query.all()
    return render_template('admin_results.html', worktasks=worktasks, resultobjects=resultobjects, rows=results.rows, cells=results.cells, columns=columns)


@app.route('/admin/results_xml/')
@admin_required
def admin_results_xml():
    """ Страница общих результатов """
    context_results = {}
    for user in users:
        res = {'first_name': users[user].get('first_name', ''),
               'last_name': users[user].get('last_name', ''),
               'second_name': users[user].get('second_name', ''),
               'birthday': users[user].get('birthday', ''),
               'teacher': users[user].get('teacher', ''),
               'school': users[user].get('school', ''),
               'class': users[user].get('class', '')}
        for task in tasks.keys():
            res[task] = 0
        context_results[user] = res

    directory = results_dir(settings)
    for filename in os.listdir(directory):
        if filename[-5:] != '.json':
            continue
        name = filename[:-5]
        (task_code, username, attempt) = name.split('-')
        res = load_json(filename, {}, directory)
        if 'results' not in res:
            continue
        task = tasks[task_code]
        total = 0
        for key, values in res['results'].items():
            task_settings = task['test_suites'][key]
            if task_settings['scoring'] == 'entire':
                total += reduce(lambda a, b: a if b['result'] == 'OK' else 0, values, task_settings['total_score'])
            elif task_settings['scoring'] == 'partial':
                total += reduce(lambda a, b: a + task_settings['test_score'] if b['result'] == 'OK' else a, values, 0)
        if context_results[username][task_code] < total:
            context_results[username][task_code] = total

    ratings = []
    for user, res in context_results.items():
        total = 0
        for task in tasks.keys():
            total += res[task]
        ratings.append({
            "user": user,
            "total": total
        })

    ratings = sorted(ratings, key=lambda r: r['total'], reverse=True)
    totals = sorted(set([r['total'] for r in ratings] + [0, 0, 0]), reverse=True)
    cups = dict(zip(totals[:3], ["I", "II", "III"]))

    context = dict(
        tasks_num=len(tasks),
        results=context_results,
        codes=sorted(tasks.keys()),
        ratings=ratings,
        cups=cups
    )

    from flask import make_response
    response = make_response(render_template('results.xml', **context))
    response.headers['Content-Disposition'] = "attachment; filename=results.xml"
    response.headers['Content-Type'] = "application/vnd.ms-excel"
    return response


if __name__ == '__main__':
    if settings.waitress:
        from waitress import serve

        serve(app, port=settings.port)
    else:
        app.run(host='0.0.0.0', port=settings.port)
