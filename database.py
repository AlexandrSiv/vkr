__author__ = 'AlexanderSivtsev'

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

username = 'postgres'
password = 'postgres'
db = 'test'
host = 'localhost'
port = '5432'

engine = create_engine('postgresql://' + username + ':' + password + '@' + host + ':' + port + '/' + db,
                       convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    Base.metadata.create_all(bind=engine)
